//
//  GenderCell.swift
//  shop-app
//
//  Created by Алем Утемисов on 20.04.16.
//  Copyright © 2016 Алем Утемисов. All rights reserved.
//

import UIKit

class GenderCell: UITableViewCell {

    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var title: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(name : String , image : String){
        title.font = UIFont(name: "HattoriHanzo-Light", size: 22)
        self.title.text = name
        self.backImage.image = UIImage(named: image)
    }
}
