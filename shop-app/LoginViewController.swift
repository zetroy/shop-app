//
//  LoginViewController.swift
//  shop-app
//
//  Created by Алем Утемисов on 04.04.16.
//  Copyright © 2016 Алем Утемисов. All rights reserved.
//

import UIKit
import Alamofire


class LoginViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var imageOnTop: UIImageView!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var loginTxtField: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var registerBtnAct: UIButton!
    @IBOutlet weak var pass2View: UIView!
    @IBOutlet weak var passwordTextField2: UITextField!
    @IBOutlet weak var menuButton : UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordTxtField.delegate = self
        loginTxtField.delegate = self
        passwordTextField2.delegate = self
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.translucent = true
        self.navigationController!.view.backgroundColor = UIColor.clearColor()
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        if revealViewController() != nil{
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func textFieldDidEndEditing(textField: UITextField) {
        if let login = loginTxtField.text, let password = passwordTxtField.text{
            if login.isEmail && password != "" && !password.containsString(" "){
                loginBtn.enabled = true
            }else{
                loginBtn.enabled = false
            }
        }
         if let login = loginTxtField.text, let pass1 = passwordTxtField.text, let pass2 = passwordTextField2.text{
            if(pass1 == pass2){
              if login.isEmail && pass1 != "" && !pass1.containsString(" "){
                registerBtnAct.enabled = true
              }else{
                registerBtnAct.enabled = false
                }
        }
        }
        
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func loginBtnTapped(sender: AnyObject) {
        
        let url = NSURL(string: "\(BASEURL)login")!
        let paremeters = [
            "email" : "\(loginTxtField.text!)",
            "password" : "\(passwordTxtField.text!)"
        ]
        Alamofire.request(.POST, url, parameters: paremeters, encoding : .JSON).responseJSON { response in
            if response.result.isSuccess{
                print("work")
                if let dict = response.result.value as? Dictionary<String, AnyObject>{
                    print(dict)
                    if let name = dict["name"] as? String, let email = dict["email"] as? String, let id = dict["_id"] as? String{
                        print(name)
                        print(email)
                        print(id)
                        let user = User(id: id, email: email, name: name)
                        let session = NSUserDefaults.standardUserDefaults()
                    
                        session.setObject(NSKeyedArchiver.archivedDataWithRootObject(user), forKey: "user")
                        session.synchronize()
                        
                        let vc =  self.storyboard!.instantiateViewControllerWithIdentifier("MainVC") as UIViewController
                        
                        self.presentViewController(vc, animated: true, completion: nil)
                        
                    }
                }
            }
        }
    }
    
    @IBAction func changeView(sender: AnyObject) {
        imageOnTop.image = UIImage(named: "regist")
        separator.hidden = true
        loginBtn.hidden = true
        registerBtn.hidden = true
        registerBtnAct.hidden = false
        pass2View.hidden = false
    }
    
    @IBAction func registerBtnTapped(sender: AnyObject) {
      //  let url = NSURL(string: "\(BASEURL)signup")!
        //let paremeters = [
        //    "email" : "\(loginTxtField.text!)",
         //   "password" : "\(passwordTxtField.text!)"
       // ]
     //   Alamofire.request(.POST, url, parameters: paremeters, encoding : .JSON).responseJSON { response in
      //      print(response.result.debugDescription)
       
           self.performSegueWithIdentifier("secondStep", sender: self )
       // }
        

    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "secondStep"{
            
            let detailVC = segue.destinationViewController as! RegistrationVC
            //let val = [loginTxtField.text! : passwordTxtField.text!]
            detailVC.parameters.append(loginTxtField.text!)
             detailVC.parameters.append(passwordTxtField.text!)
        }
    }
}
