//
//  ItemCell.swift
//  shop-app
//
//  Created by Алем Утемисов on 24.04.16.
//  Copyright © 2016 Алем Утемисов. All rights reserved.
//

import UIKit

class ItemCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
