//
//  Item.swift
//  shop-app
//
//  Created by Алем Утемисов on 24.04.16.
//  Copyright © 2016 Алем Утемисов. All rights reserved.
//

import Foundation

class Item{
    var id : Int!
    var name : String!
    var image : String!
    init(id : Int, name : String, image : String){
        self.id = id
        self.name = name
        self.image = image
    }
    
}