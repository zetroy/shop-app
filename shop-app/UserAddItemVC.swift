//
//  UserAddItemVC.swift
//  shop-app
//
//  Created by Алем Утемисов on 25.04.16.
//  Copyright © 2016 Алем Утемисов. All rights reserved.
//

import UIKit
import Alamofire
class UserAddItemVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var descriptionTxtField: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.translucent = true
        self.navigationController!.view.backgroundColor = UIColor.clearColor()
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        let imageRec = UITapGestureRecognizer(target: self, action: #selector(RegistrationVC.imageTapped));
        self.imageView.addGestureRecognizer(imageRec)
        
       
    }
    
    @IBAction func addBtnPressed(sender: AnyObject) {
        let myparameters = [
            "name" : "\(nameTxtField.text!)",
            "description" : "\(descriptionTxtField.text!)",
            ]
            let url = NSURL(string: "\(BASEURL)items")!
      
        
        let image = imageView.image!
        Alamofire.upload(.POST, url,
                         // define your headers here
            headers: ["Authorization": "auth_token"],
            multipartFormData: { multipartFormData in
                
                // import image to request
                if let imageData = UIImageJPEGRepresentation(image, 1) {
                    multipartFormData.appendBodyPart(data: imageData, name: "image", fileName: "myImage.png", mimeType: "image/png")
                }
                
                // import parameters
                for (key, value) in myparameters {
                    multipartFormData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
                }
            }, // you can customise Threshold if you wish. This is the alamofire's default value
            encodingMemoryThreshold: Manager.MultipartFormDataEncodingMemoryThreshold,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .Success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint(response)
                    }
                case .Failure(let encodingError):
                    print(encodingError)
                }
        })

        
    }
    
    func imageTapped(){
        
        let alert = UIAlertController(title: "", message: "Откуда добавить фото", preferredStyle: UIAlertControllerStyle.Alert)
        let cameraView = UIImagePickerController()
        cameraView.delegate = self
        alert.addAction(UIAlertAction(title: "Фото", style: .Default, handler: { action in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
                
                
                
                cameraView.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                cameraView.allowsEditing = false
                self.presentViewController(cameraView, animated: true, completion: nil)
                
            }
            
        }))
        alert.addAction(UIAlertAction(title: "Камера", style: .Default, handler: { action in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                
                
                cameraView.sourceType = UIImagePickerControllerSourceType.Camera
                self.presentViewController(cameraView, animated: true, completion: nil)
                
            }
            
            
        }))
        alert.addAction(UIAlertAction(title: "Отмена", style: .Cancel, handler: { action in
            
            
        }))
        self.presentViewController(alert, animated: true, completion: nil)
        
        
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        self.imageView.image = image
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
    
    
    
}
