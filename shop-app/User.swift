//
//  User.swift
//  shop-app
//
//  Created by Алем Утемисов on 18.04.16.
//  Copyright © 2016 Алем Утемисов. All rights reserved.
//

class User : NSObject, NSCoding{
    private var _id : String!
    private var _email : String!
    private var _name : String!
    var name : String{
        return self._name
    }
    var id : String {
        return self._id
    }
    var email : String{
        return self._email
    }
    init(id : String, email : String , name : String){
        self._id = id
        self._email = email
        self._name = name
    }
    override init() {
        
    }
    required init(coder aDecoder: NSCoder) {
        self._id = aDecoder.decodeObjectForKey("_id") as! String
        self._name = aDecoder.decodeObjectForKey("_name") as! String
        self._email = aDecoder.decodeObjectForKey("_email") as! String
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        
            aCoder.encodeObject(_id, forKey: "_id")
            aCoder.encodeObject(_name, forKey: "_name")
            aCoder.encodeObject(_email, forKey: "_email")
        
    }
    
}

//SUCCESS: {
//    "__v" = 0;
//    "_id" = 5701f7a9e76b208c1069339b;
//    email = "admin@mail.ru";
//    name = Yelnur;
//    password = "$2a$10$hXnZ84EYR/P5Ga33JIa.ie4M5vdu0gZq2WWunwS5Y0he6dwJ0pQpu";
//}''

