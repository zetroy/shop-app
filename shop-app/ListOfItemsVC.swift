//
//  ListOfItemsVC.swift
//  shop-app
//
//  Created by Алем Утемисов on 24.04.16.
//  Copyright © 2016 Алем Утемисов. All rights reserved.
//

import UIKit
class ListOfItemsVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{


    @IBOutlet weak var segmentControl: UICollectionView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    let model = ["Все","Футболки","Носки","Майки","Белье"]
    
    var catId : Int!
    
    override func viewDidLoad() {
        
        collectionView.dataSource = self
        collectionView.delegate = self
        segmentControl.dataSource = self
        segmentControl.delegate = self
        collectionView.registerNib(UINib(nibName: "ItemCell", bundle: nil), forCellWithReuseIdentifier: "ItemCell")
        segmentControl.registerNib(UINib(nibName: "CellForSegment",bundle: nil), forCellWithReuseIdentifier: "CellForSegment")
        
        super.viewDidLoad()
    }
    override func viewWillAppear(animated: Bool) {
       self.navigationController!.navigationBar.backItem?.title = ""
    }
    override func viewDidAppear(animated: Bool) {
        self.navigationController!.navigationBar.backItem?.title = ""
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == segmentControl{
             return model.count
        }else{
            return 5
        }
       
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if collectionView == self.segmentControl{
        
        }else{
            performSegueWithIdentifier("toItem", sender: indexPath.row)
        }
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
       
        if collectionView == self.segmentControl{
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CellForSegment",forIndexPath: indexPath) as! CellForSegment
            cell.nameLbl.font = UIFont(name: "HattoriHanzo-Light", size: 22)
            cell.nameLbl.text = model[indexPath.row]
            return cell
            
        }else {
           let  cell = collectionView.dequeueReusableCellWithReuseIdentifier("ItemCell",forIndexPath: indexPath) as! ItemCell
            cell.nameLbl.text = "Вещь"
            cell.nameLbl.font = UIFont(name: "HattoriHanzo-Light", size: 22)
            cell.imageView.image = UIImage(named: "rubashka")
            return cell
            
        }
       
        
        
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if collectionView == self.segmentControl{
            return CGSizeMake(collectionView.frame.width/3.5, collectionView.frame.height)
        }else{
            return CGSizeMake(collectionView.frame.width / 2.1, collectionView.frame.width / 2.5)

        }
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toItem"{
            if let vc = segue.destinationViewController as? ItemVC{
                vc.id = sender as? Int
            }
            
        }
    }
   }


