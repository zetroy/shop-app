//
//  pageView.swift
//  shop-app
//
//  Created by Алем Утемисов on 23.04.16.
//  Copyright © 2016 Алем Утемисов. All rights reserved.
//

import UIKit

class pageView: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    var index : Int = 0
    var imageName: String = "" {
        
        didSet {
            
            if let imgView = imageView {
                imgView.image = UIImage(named: imageName)
            }
            
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView!.image = UIImage(named: imageName)
    }


}
