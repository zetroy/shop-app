//
//  ImagePVC.swift
//  shop-app
//
//  Created by Алем Утемисов on 23.04.16.
//  Copyright © 2016 Алем Утемисов. All rights reserved.
//

import UIKit

class ImagePVC:  UIViewController, UIPageViewControllerDataSource {

    var adds = [Advertisement]()
    var pageControl : UIPageControl = UIPageControl()
    private var pageViewController: UIPageViewController?
    var index : Int = 0
    var timer = NSTimer()
    override func viewDidLoad() {
        super.viewDidLoad()
          timer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: #selector(ImagePVC.update), userInfo: nil, repeats: true)
        adds.append(Advertisement(id: 0, name: "page1", adText: "Скидки в магазине ZARA до 30%"))
        adds.append(Advertisement(id: 1, name: "page2", adText: "privet"))
        adds.append(Advertisement(id: 2, name: "qwert", adText: "privet"))
        pageControl.numberOfPages = adds.count
        let pageController = self.storyboard!.instantiateViewControllerWithIdentifier("PageController") as! UIPageViewController
        pageController.dataSource = self
        if adds.count > 0{
            let firstController = getItemController(0)!
            self.index = firstController.index
            let startingViewControllers: NSArray = [firstController]
            pageController.setViewControllers(startingViewControllers as? [UIViewController], direction: .Forward, animated: false, completion: nil)
        }
        pageViewController = pageController
        self.pageViewController!.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+40);
        self.pageViewController!.view.addSubview(pageControl)
        self.view.addSubview(pageViewController!.view)
        
    }
       private func getItemController(itemIndex: Int) -> pageView? {
        
        if itemIndex < adds.count && itemIndex >= 0 {
            let page = self.storyboard!.instantiateViewControllerWithIdentifier("pageView") as! pageView
            page.index = adds[itemIndex].id
            pageControl.currentPage = adds[itemIndex].id
            page.imageName = adds[itemIndex].imageName
            return page
        }
        
        return nil
    }

    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        let itemController = viewController as! pageView
        
        if itemController.index < adds.count && self.index + 1 < adds.count {
            return getItemController(itemController.index + 1)
        }
        return nil
        
        
    }
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        let itemController = viewController as! pageView
        if itemController.index > 0 {
            return getItemController(itemController.index - 1)
        }
        
        return nil
    }
    func update(){
         if self.index + 1 < adds.count {
        let pvcs = pageViewController?.childViewControllers as! [pageView]
        let itemIndex = pvcs[index].index
        let firstController = getItemController(itemIndex+1)!
        let startingViewControllers = [firstController]
       
            self.index += 1
        
        pageViewController!.setViewControllers(startingViewControllers, direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: nil)
         
    }
    }
   
}
