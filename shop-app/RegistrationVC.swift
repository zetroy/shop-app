//
//  RegistrationVC.swift
//  shop-app
//
//  Created by Алем Утемисов on 18.04.16.
//  Copyright © 2016 Алем Утемисов. All rights reserved.
//

import UIKit
import Alamofire
class RegistrationVC: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewDelegate, UIPickerViewDataSource {

    var parameters = [String]()
    
    
    
    @IBOutlet weak var regBtn: UIButton!
    @IBOutlet weak var trcPicker: UIPickerView!
    @IBOutlet weak var imageOnTop: UIImageView!
    @IBOutlet weak var shopName: UITextField!
    @IBOutlet weak var descriptionTxtField: UITextField!
    @IBOutlet weak var locationTxtField: UITextField!
    var id : String = ""
    var malls =  [Malls]()
    override func viewWillAppear(animated: Bool) {
      
    }
    override func viewDidLoad() {
        
        super.viewDidLoad()
    
        self.navigationController!.navigationBar.backItem?.title = ""
        self.trcPicker.dataSource = self
        self.trcPicker.delegate = self
        let imageRec = UITapGestureRecognizer(target: self, action: #selector(RegistrationVC.imageTapped));
        self.imageOnTop.addGestureRecognizer(imageRec)
      
       print(malls)
    }
    func imageTapped(){
        
        let alert = UIAlertController(title: "", message: "Откуда добавить фото", preferredStyle: UIAlertControllerStyle.Alert)
         let cameraView = UIImagePickerController()
        cameraView.delegate = self
        alert.addAction(UIAlertAction(title: "Фото", style: .Default, handler: { action in
         
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
               
                
                
                cameraView.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                cameraView.allowsEditing = false
                self.presentViewController(cameraView, animated: true, completion: nil)
                
            }

        }))
        alert.addAction(UIAlertAction(title: "Камера", style: .Default, handler: { action in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
               
                
                cameraView.sourceType = UIImagePickerControllerSourceType.Camera
                self.presentViewController(cameraView, animated: true, completion: nil)
                
            }

            
        }))
        alert.addAction(UIAlertAction(title: "Отмена", style: .Cancel, handler: { action in
            
            
        }))
        self.presentViewController(alert, animated: true, completion: nil)

        
          }
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 5
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let st = "ТРЦ"
        return "ТРЦ"
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.id = malls[row].id
        self.view.endEditing(true)
    }
    
    
    @IBAction func register(sender: AnyObject) {
        //Alamofire.request(<#T##URLRequest: URLRequestConvertible##URLRequestConvertible#>)
        regBtn.enabled = false
         let url = NSURL(string: "\(BASEURL)signup")!
        
         
        let image = imageOnTop.image!
        // define parameters
      
        let myparameters = [
            "email" : "\(parameters[0])",
            "password" : "\(parameters[1])",
           "name" : "\(self.shopName.text!)",
        //"mall" : self.id,
            "description" : self.descriptionTxtField.text!,
            "location" : self.locationTxtField.text!,
        ]
        // Begin upload
        Alamofire.upload(.POST, url,
                         // define your headers here
            headers: ["Authorization": "auth_token"],
            multipartFormData: { multipartFormData in
                
                // import image to request
                if let imageData = UIImageJPEGRepresentation(image, 1) {
                    multipartFormData.appendBodyPart(data: imageData, name: "image", fileName: "myImage.png", mimeType: "image/png")
                }
                
                // import parameters
                for (key, value) in myparameters {
                    multipartFormData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key as! String)
                }
            }, // you can customise Threshold if you wish. This is the alamofire's default value
            encodingMemoryThreshold: Manager.MultipartFormDataEncodingMemoryThreshold,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .Success(let upload, _, _):
                    upload.responseJSON { response in
                            print("work")
                            if let dict = response.result.value as? Dictionary<String, AnyObject>{
                                print(dict)
                                if let name = dict["name"] as? String, let email = dict["email"] as? String, let id = dict["_id"] as? String{
                                    print(name)
                                    print(email)
                                    print(id)
                                    let user = User(id: id, email: email, name: name)
                                    let session = NSUserDefaults.standardUserDefaults()
                                    
                                    session.setObject(NSKeyedArchiver.archivedDataWithRootObject(user), forKey: "user")
                                    session.synchronize()
                                    
                                    let vc =  self.storyboard!.instantiateViewControllerWithIdentifier("MainVC") as UIViewController
                                    
                                    self.presentViewController(vc, animated: true, completion: nil)
                                    
                                }
                            }
                        }

                case .Failure(let encodingError):
                    print(encodingError)
                }
        })
        

    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
    func setData(){
        
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        self.imageOnTop.image = image
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
}
