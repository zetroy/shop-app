//
//  ManViewController.swift
//  shop-app
//
//  Created by Алем Утемисов on 03.04.16.
//  Copyright © 2016 Алем Утемисов. All rights reserved.
//

import UIKit

class ManViewController: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    

    var gender : Int!
    @IBOutlet weak var collection : UICollectionView!
    var titleForController : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collection.delegate = self
        collection.registerNib(UINib(nibName: "CustomCellForCategory", bundle: nil), forCellWithReuseIdentifier: "catcell")
        collection.dataSource = self
        
            // Do any additional setup after loading the view.
        self.title = titleForController
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.translucent = true
        self.navigationController!.view.backgroundColor = UIColor.clearColor()
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        
    }
    override func viewWillAppear(animated: Bool) {
        self.navigationController!.navigationBar.backItem?.title = ""
    }

    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("catcell", forIndexPath: indexPath) as! CustomCellForCategory
        //cell.configureCell("ASDaSDfs", image: "tigr")
       cell.nameLbl.text = "Моя подкатегория"
        cell.nameLbl.font = UIFont(name: "HattoriHanzo-Light", size: 22)
        cell.imageView.image = UIImage(named: "sumka")
        return cell
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(collection.frame.width / 2.1, collection.frame.width / 2.5)
    }
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
     performSegueWithIdentifier("toList", sender: indexPath.row)
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toList"{
            if let vc = segue.destinationViewController as? ListOfItemsVC{
                vc.catId = sender as? Int
                vc.navigationController?.navigationBar.backItem?.title = ""
            }
            
    }
    }
 
    
    
}
