//
//  SideBarVC.swift
//  shop-app
//
//  Created by Алем Утемисов on 17.04.16.
//  Copyright © 2016 Алем Утемисов. All rights reserved.
//

import UIKit

class SideBarVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var mytableView: UITableView!
    @IBOutlet weak var bottomBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mytableView.delegate = self
        mytableView.dataSource = self
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    // MARK: - Table view data source
    override func viewWillAppear(animated: Bool) {
        let session = NSUserDefaults.standardUserDefaults();
        if session.objectForKey("user") != nil {
            bottomBtn.setTitle("Профиль", forState: .Normal)
        }else{
        bottomBtn.setTitle("Авторизация", forState: .Normal)
        }
    }
     func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return " "
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("sidebarcell", forIndexPath: indexPath)
        cell.textLabel?.font = UIFont(name: "HattoriHanzo-Light", size: 18)
        if indexPath.row == 0 {
            cell.textLabel?.text = "Домашняя"
        } else{
//            let session = NSUserDefaults.standardUserDefaults()
//            let decoded  = session.objectForKey("user") as! NSData
//            let user = NSKeyedUnarchiver.unarchiveObjectWithData(decoded) as! User
   cell.textLabel?.text = "Esentai"
            
        }
        return cell
    }
     func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
     if indexPath.row == 0 {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        
        let vc : UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("MainVC") as UIViewController
        self.showViewController(vc, sender: self)
        
        }
        
    }
    @IBAction func bottomBtnPressed() {
        let session = NSUserDefaults.standardUserDefaults()
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        var vc : UIViewController
        if (session.objectForKey("user") != nil){
            vc = mainStoryboard.instantiateViewControllerWithIdentifier("UserVC") as UIViewController
        }else{
            vc = mainStoryboard.instantiateViewControllerWithIdentifier("LoginViewController") as UIViewController
            
        }
       self.showViewController(vc, sender: self)
    }


}
