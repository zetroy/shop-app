//
//  Advertisement.swift
//  shop-app
//
//  Created by Алем Утемисов on 23.04.16.
//  Copyright © 2016 Алем Утемисов. All rights reserved.
//

import Foundation

class Advertisement {
    let id : Int!
    let imageName : String!
    let adText : String!
    init(id : Int, name : String , adText : String){
        self.id = id
        self.imageName = name
        self.adText = adText
    }
}