//
//  CustomCellForCategory.swift
//  shop-app
//
//  Created by Алем Утемисов on 18.04.16.
//  Copyright © 2016 Алем Утемисов. All rights reserved.
//

import UIKit

class CustomCellForCategory: UICollectionViewCell {
    @IBOutlet weak var nameLbl : UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    
    func configureCell(name : String, image : String){
        nameLbl.text = name
        
          imageView.image = UIImage(named: image)
        //imageView.frame.size = CGSizeMake(self.frame.width, self.frame.height)
        // imageView.contentMode = UIViewContentMode.ScaleAspectFit
        
        
        
    }
}
