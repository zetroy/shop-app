//
//  ViewController.swift
//  shop-app
//
//  Created by Алем Утемисов on 03.03.16.
//  Copyright © 2016 Алем Утемисов. All rights reserved.
//

import UIKit
import Alamofire
class ViewController: UIViewController, UITableViewDelegate,UITableViewDataSource{

 
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var menuButton : UIBarButtonItem!
    
    let categories = [ "Женская","Мужская","Детская"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        tableView.registerNib(UINib(nibName: "GenderCell", bundle: nil), forCellReuseIdentifier: "genderCell")
        self.tableView.delegate = self
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.translucent = true
        self.navigationController!.view.backgroundColor = UIColor.clearColor()
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        navigationController!.navigationBar.backItem?.title = ""
        if revealViewController() != nil{
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
       // let session = NSUserDefaults.standardUserDefaults()
        //let decoded  = session.objectForKey("user") as! NSData
        //let user = NSKeyedUnarchiver.unarchiveObjectWithData(decoded) as! User
        //print(user.name)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
        performSegueWithIdentifier("toGender", sender: indexPath.row)
    
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return tableView.frame.height / 3
        
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("genderCell", forIndexPath: indexPath) as! GenderCell
        cell.configureCell(categories[indexPath.row], image: "\(indexPath.row)")
        

        return cell
        
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toGender"{
            if let catVC = segue.destinationViewController as? ManViewController{
                catVC.gender = sender as? Int
                catVC.titleForController = categories[sender as! Int]
                catVC.navigationController?.navigationBar.backItem?.title = ""
            }
        }
    }    

}
