//
//  ItemVC.swift
//  shop-app
//
//  Created by Алем Утемисов on 24.04.16.
//  Copyright © 2016 Алем Утемисов. All rights reserved.
//

import UIKit

class ItemVC: UIViewController {
    var id : Int!
    
    var phoneNumber : Int!
    @IBOutlet weak var imageVIew: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var descriptionTextField: UITextView!
    @IBOutlet weak var shopNameLbl: UILabel!
    @IBOutlet weak var trcNameLbl: UILabel!
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    

    @IBAction func call(sender: AnyObject) {
        let alert = UIAlertController(title: "", message: "Позвонить по номеру \(self.phoneNumber)", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Набрать", style: .Default, handler: { action in
            let url:NSURL = NSURL(string: "tel://\(87715806890)")!
            UIApplication.sharedApplication().openURL(url)
        }))
        alert.addAction(UIAlertAction(title: "Отмена", style: .Cancel, handler: { action in
            
        }))
        self.presentViewController(alert, animated: true, completion: nil)
       
    }
}
