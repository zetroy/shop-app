//
//  Constants.swift
//  shop-app
//
//  Created by Алем Утемисов on 11.04.16.
//  Copyright © 2016 Алем Утемисов. All rights reserved.
//

import Foundation

extension String {
var isEmail: Bool {
    do {
        let regex = try NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .CaseInsensitive)
        return (regex.firstMatchInString(self, options: NSMatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil).boolValue
    } catch {
        return false
    }
}
   
}
typealias DownloadComplete = () -> ()
 let BASEURL = "https://malls-yelnur.c9users.io/api/"
